const aes256 = require('./aes256');
const Hashes = require('./hashes');

const oneMinuteInMillies = 60 * 1000;
const signatureKey = "X-getplace-signature";

exports.validator = (pass) => {
        calculateSignature: (body) => {
            const md5 = new Hashes.MD5().hex(body);
            const now = new Date().getTime();
            const payload = { hash:md5, timestamp:now};
            const payloadAsString = JSON.stringify(payload);
            const encryptedPayload = aes256.encrypt(pass, payloadAsString);
            return { [signatureKey]: encryptedPayload };
        },
        
        validateRequestAutheticity: (request) => {
            if (!request || !request.body || !request.headers) {
                return false;
            }

            const body = request.body;
            const signature = request.headers[signatureKey];
            
            if (!body || !signature || typeof signature !== 'string') {
                return false;
            }
            
            const decryptedPayload = aes256.decrypt(pass, signature);
            try {
                const payLoadAsJSON = JSON.parse(decryptedPayload);
                if (!payLoadAsJSON.hash || !payLoadAsJSON.timestamp) {
                    return false;
                }

                if (new Date().getTime() - payLoadAsJSON.timestamp > oneMinuteInMillies) {
                    return false;
                }

                return new Hashes.MD5().hex(body) === payLoadAsJSON.hash;
            }
            catch(e) {
                return false;
            }
        }
    }